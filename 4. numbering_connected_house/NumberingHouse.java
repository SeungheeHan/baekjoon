import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

//문제: https://www.acmicpc.net/problem/2667
//풀이1: https://raspberrypi98.tistory.com/68
//풀이2: https://n1tjrgns.tistory.com/245
//풀이3: https://ballpython.tistory.com/7
//내풀이는 3번에 가깝고, cnt를 카운팅 하는 변수 선언만 풀이1을 따른 정도

//입력:
// 7
// 0110100
// 0110101
// 1110101
// 0000111
// 0100000
// 0111110
// 0111000

//출력:
// 3
// 7
// 8
// 9
class NumberingHouse {
    static int[][] map;
    static int[][] visited;
    static int cnt;
    static int[] delta_row = {0, +1, 0, -1};
    static int[] delta_col = {-1, 0, +1, 0};
    static int n_row_col = 0;
    static ArrayList<Integer> num_connected_house = new ArrayList<Integer>();

    public static void main(String args[]) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        // make map
        n_row_col = Integer.parseInt(br.readLine());
        map = new int[n_row_col][n_row_col];
        visited = new int[n_row_col][n_row_col];
        for(int row=0; row<n_row_col; row++) {
            String line = br.readLine();
            for(int col=0; col<n_row_col; col++) {
                map[row][col] = line.charAt(col) - '0';
                visited[row][col] = 0;
            }
        }

        // find house
        for(int row=0; row<n_row_col; row++) {
            for(int col=0; col<n_row_col; col++) {
                if (map[row][col] == 1 && visited[row][col] == 0) {
                    cnt = 1;
                    find_house(row, col);
                    num_connected_house.add(cnt);
                }
            }
        }

        Collections.sort(num_connected_house);
        System.out.println(num_connected_house.size());
        for(int i=0; i<num_connected_house.size(); i++) {
            System.out.println(num_connected_house.get(i));
        }
    }

    static void find_house(int row, int col) {
        visited[row][col] = 1;
        for(int i=0; i<4; i++) {
            int moved_row = row + delta_row[i];
            int moved_col = col + delta_col[i];
            if ((moved_row >= 0 && moved_row < n_row_col) &&
                (moved_col >= 0 && moved_col < n_row_col) &&
                visited[moved_row][moved_col] == 0 &&
                map[moved_row][moved_col] == 1) {
                find_house(moved_row, moved_col);
                cnt++;
            }
        }
    }
}
