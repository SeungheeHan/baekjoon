import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 문제: https://www.acmicpc.net/problem/2225
// 풀이1(top-down, bottom-up 모두 있음): https://softworking.tistory.com/m/40
// 풀이2: https://mygumi.tistory.com/135
class SplitSum {
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        StringTokenizer st = new StringTokenizer(line);
        
        int N = Integer.parseInt(st.nextToken());
        int K = Integer.parseInt(st.nextToken());

        int[][] counter = new int[N+1][K+1];
        for (int c=1; c<=K; c++) {
            counter[0][c] = 1;
            for (int n=1; n<=N; n++) {
                // counter[n][c] = (counter[n - 1][c] + counter[n][c - 1]) % 1000000000;
                counter[n][c] = (counter[n-1][c] + counter[n][c-1]);
            }
        }

        System.out.println(counter[N][K]);
    }
}