import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

class ConnectedGraph {
    static ArrayList<Integer> [] graph;
    static boolean visited [];
    public static void main(String args[]) throws IOException {
        // 풀이: https://hees-dev.tistory.com/m/46?category=824514
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int num_vertex = Integer.parseInt(st.nextToken());
        int num_edge = Integer.parseInt(st.nextToken());

        graph = new ArrayList[num_vertex+1];
        visited = new boolean[num_vertex+1];
        int vertex1 = 0;
        int vertex2 = 0;
        int num_sub_graph = 0;

        for(int i=0; i<num_vertex+1; i++) {
            graph[i] = new ArrayList<Integer>();
        }

        for(int i=0; i< num_edge; i++) {
            st = new StringTokenizer(br.readLine());
            vertex1 = Integer.parseInt(st.nextToken());
            vertex2 = Integer.parseInt(st.nextToken());
            graph[vertex1].add(vertex2);
            graph[vertex2].add(vertex1);
        }

        for(int i = 1; i < num_vertex+1; i++) {
			if(!visited[i]) {
				find_connected_vertex(i);
				num_sub_graph++;
			}
        }

		System.out.println(num_sub_graph);
    }

    static void find_connected_vertex(int vertex) {
        if (visited[vertex]) {
            return ;
        }
        visited[vertex] = true;
        for(int i: graph[vertex]) {
            if(!visited[i]) {
                find_connected_vertex(i);
            }
        }

    }
}