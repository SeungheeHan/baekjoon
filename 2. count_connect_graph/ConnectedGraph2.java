import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.ArrayList;


public class ConnectedGraph2 {
    static ArrayList<Integer> [] graph;
    static boolean [] visited;
    static int count = 0;
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        
        int numVertex = Integer.parseInt(st.nextToken());
        int numEdge = Integer.parseInt(st.nextToken());
        
        graph = new ArrayList[numVertex+1];
        visited = new boolean[numVertex+1];
        for(int i=0; i<numVertex+1; i++){
            graph[i] = new ArrayList<Integer>();
        }
        
        int vert1 = 0;
        int vert2 = 0;
        for(int i=0; i<numEdge; i++) {
            st = new StringTokenizer(br.readLine());
            vert1 = Integer.parseInt(st.nextToken());
            vert2 = Integer.parseInt(st.nextToken());
            graph[vert1].add(vert2);
            graph[vert2].add(vert1);
        }

        for(int i=1; i<numVertex+1; i++){
            if(visited[i]) continue;
            find_connected_vert(i);
            count++;
        }

        System.out.println(count);
    }
    
    public static void find_connected_vert(int i) {
        if(visited[i]) return;
        visited[i] = true;

        for(int tempVert : graph[i]) {
            if(visited[tempVert]) continue;
            find_connected_vert(tempVert);
        }
    }
}
