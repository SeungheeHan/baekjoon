import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class AddOneTwoThree {
    // 문제: https://www.acmicpc.net/problem/9095
    // 풀이: https://zorba91.tistory.com/m/43
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int T = Integer.parseInt(line);
        int N;

        int[] output = new int[11]; // 동적으로 추가되게하니까, 메모리 초과 발생해서 정적으로. <= 동적으로 만들면 왜 메모리 초과가 날까?
        output[1] = 1; // 1을 넣을 경우의 수 1가지: 1
        output[2] = 2; // 2를 넣을 경우의 수 2가지: 1+1, 2
        output[3] = 4; // 3을 넣을 경우의 수 4가지: 1+1+1, 1+2, 2+1, 3

        for(int i=0; i<T; i++) {
            N = Integer.parseInt(br.readLine());
            for(int j=0; j<=N; j++) {
                output[j] = output[j-1] + output[j-2] + output[j-3];
            }
            System.out.println(output[N]);
        }

    }
}