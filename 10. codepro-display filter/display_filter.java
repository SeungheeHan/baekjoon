import java.util.Scanner;


public class display_filter {
	int N;//필터의 수
	int R[]=new int [11]; //반사의 정도
	int P[]=new int [11]; //투과의 정도
	
	int mincount;
	long mindiff;
	
	void InputData(){
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		for (int i = 1; i <= N; i++) {
			R[i] = sc.nextInt();
			P[i] = sc.nextInt();
		}
		sc.close();
	}
	
	long ABS(long x) {
		return (x < 0) ? -x : x;
	}

	void DFS(int start, int count, long mul, long sum) {
		if(count != 0) {
			long diff = ABS(mul - sum);
			if((mindiff > diff) || ((mindiff == diff) && (mincount > count))) {
				mindiff = diff;
				mincount = count;
			}
		}
		
		for(int i=start; i<=N; i++) {
			DFS(i+1, count+1, mul*R[i], sum+P[i]);
		}
	}
	
	int Solve() {
		mindiff = (long)1e6;
		DFS(1, 0, 1, 0);
		return N - mincount;
	}

	public static void main(String[] args){
		int ans = -1;
		display_filter m = new display_filter();

		m.InputData();	 //	입력 함수

		//	코드를 작성하세요
		ans = m.Solve();
		
		System.out.println(ans);//출력
		
	}
}