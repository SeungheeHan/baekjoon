import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

public class CountIsland4{
	public static int ISLAND[][];
	public static int numRow, numCol;
	public static int count;
	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));
		String line;
		StringBuilder resList = new StringBuilder();

		
		while(true) {
			count = 0;
			line = in.readLine();
			numRow = Integer.parseInt(line.split(" ")[0]);
			numCol = Integer.parseInt(line.split(" ")[1]);
			
			if(numRow == 0 && numCol == 0) break;

			ISLAND = new int[numRow][numCol];
			for(int i=0; i<numRow; i++){
				line = in.readLine();
				String [] splitLine = line.split(" ");
				for(int j=0; j<numCol; j++){
					ISLAND[i][j] = Integer.parseInt(splitLine[j]);
				}
			}

			for(int i=0; i<numRow; i++){
				for(int j=0; j<numCol; j++){
					if(ISLAND[i][j] > 0) {
						count++;
						search(i, j);
					}
				}
			}

			resList.append(count + " ");
		}

		out.write(resList.toString());
		out.close();
		in.close();
	}

	public static void search(int row, int col) {
		ISLAND[row][col] = 0;
		int movingRow[] = {0,1,1,1,0,-1,-1,-1};
		int movingCol[] = {1,1,0,-1,-1,-1,0,1};
		int deltaRow, deltaCol;

		for(int i=0; i<8; i++) {
			deltaRow = row + movingRow[i];
			deltaCol = col + movingCol[i];
			
			if((deltaRow >= 0 && deltaRow < numRow) && (deltaCol >= 0 && deltaCol < numCol) && (ISLAND[deltaRow][deltaCol] > 0)) {
				search(deltaRow, deltaCol);
			}
		}
	}
}
