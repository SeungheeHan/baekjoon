import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

// 풀이: http://blog.naver.com/PostView.nhn?blogId=occidere&logNo=220902505227
// 배열 col, row 길이 세기: https://dojang.io/mod/page/view.php?id=310
public class CountIsland {
    static int map[][] = null;
    static int width = 0;
    static int height = 0;

    public static void main(String args[]) {
        File file = new File("/Users/alphahacker/algorithm/count_island/input_file.txt");
        FileReader filereader;
        try {
            filereader = new FileReader(file);
            BufferedReader br = new BufferedReader(filereader);
            String line = "";
            StringTokenizer st = null;

            int line_count = 0;
            while ((line = br.readLine()) != null) {
                st = new StringTokenizer(line);

                if (line_count == 0) {
                    width = Integer.parseInt(st.nextToken());
                    height = Integer.parseInt(st.nextToken());
                    map = new int[height][width];
                }
                else{
                    for(int i=0; i<width; i++) {
                        map[line_count-1][i] = Integer.parseInt(st.nextToken());
                    }
                }
                line_count++;
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        find_land(map);
    }

    private static void find_land(int map[][]) {
        int num_map = 0;
        for(int r=0; r<height; r++) {
            for(int c=0; c<width; c++) {
                if(map[r][c] > 0) {
                    num_map++;
                    search_connected_land(map, r, c);
                }
            }
        }
        System.out.println("# of islands: " + num_map);
    }

    private static void search_connected_land(int map[][], int row_index, int col_index) {
        map[row_index][col_index] = 0;
        int i, ax[] = {0,1,1,1,0,-1,-1,-1}, ay[] = {1,1,0,-1,-1,-1,0,1}, dx, dy;
        for(i=0;i<8;i++) {
            dx = row_index + ax[i];
            dy = col_index + ay[i];
            if(is_in_range(dx, dy) && map[dx][dy]>0) {
                search_connected_land(map, dx, dy);
            }
        }
    }

    private static boolean is_in_range(int x, int y){
        return (0<=x && x<height) && (0<=y && y<width);
    }
}

// 1 1
// 0

// 2 2
// 0 1
// 1 0

// 3 2
// 1 1 1
// 1 1 1

// 5 4
// 1 0 1 0 0
// 1 0 0 0 0
// 1 0 1 0 1
// 1 0 0 1 0

// 5 4
// 1 1 1 0 1
// 1 0 1 0 1
// 1 0 1 0 1
// 1 0 1 1 1

// 5 5
// 1 0 1 0 1
// 0 0 0 0 0
// 1 0 1 0 1
// 0 0 0 0 0
// 1 0 1 0 1
