import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.io.BufferedReader;

public class CounIsland2 {
    static int map[][];
    static int num_column;
    static int num_row;

    public static void main(String args[]) throws NumberFormatException, IOException {
        File file = new File("/Users/alphahacker/algorithm/count_island/input_file.txt");
        FileReader filereader = new FileReader(file);

        BufferedReader br = new BufferedReader(filereader);
        String line = "";
        StringTokenizer st = null;

        int line_count = 0;
        while((line = br.readLine()) != null) {
            st = new StringTokenizer(line);
            if (line_count == 0) {
                num_column = Integer.parseInt(st.nextToken());
                num_row = Integer.parseInt(st.nextToken());
                map = new int[num_row][num_column];
            }
            else {
                for(int i=0; i<num_column; i++) {
                    map[line_count-1][i] = Integer.parseInt(st.nextToken());
                }
            }
            line_count++;
        }

        System.out.println("# of island: " + find_land());
    }

    static int find_land() {
        int num_island = 0;
        for(int i=0; i<num_row; i++) {
            for(int j=0; j<num_column; j++) {
                if(map[i][j] > 0) {
                    num_island++;
                    find_connected_land(i, j);
                }
            }
        }
        return num_island;
    }

    static void find_connected_land(int curr_row, int curr_col) {
        map[curr_row][curr_col] = 0;
        int moving_row[] = {-1, -1, 0, 1, 1, 1, 0, -1};
        int moving_col[] = {0, 1, 1, 1, 0, -1, -1, -1};
        for(int i=0; i<8; i++) {
            int changed_row = curr_row + moving_row[i];
            int changed_col = curr_col + moving_col[i];
            if (is_in_range(changed_row, changed_col) && map[changed_row][changed_col] > 0) {
                find_connected_land(changed_row, changed_col);
            }
        }
    }

    static boolean is_in_range(int row, int col) {
        return (0 <= row && row < num_row) && (0 <= col && col < num_column);
    }
}