import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;

// 다익스트라 설명: https://hsp1116.tistory.com/42
// 풀이: https://m.blog.naver.com/PostView.nhn?blogId=occidere&logNo=220920119726&proxyReferer=https:%2F%2Fwww.google.com%2F
public class Algospot1 {
    public static int map [][];
    public static int rowSize, colSize;
    public static int [] dRow = {1, -1, 0, 0};
    public static int [] dCol = {0, 0, -1, 1};
    public static void main(String[] args) throws IOException {
        // 입력 예시
        // 3 3
        // 011
        // 111
        // 110

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        
        String line = br.readLine();
        String [] splited_line = line.split(" ");
        rowSize = Integer.parseInt(splited_line[0]);
        colSize = Integer.parseInt(splited_line[1]);
        
        map = new int[rowSize][colSize];
        
        for(int i=0; i<rowSize; i++) {
            line = br.readLine();
            for(int j=0; j<line.length(); j++) {
                map[i][j] = line.charAt(j) - '0';
            }
        }

        // for(int i=0; i<rowSize; i++) {
        //     for(int j=0; j<colSize; j++) {
        //         System.out.println(map[i][j]);
        //     } 
        //     System.out.println();
        // }

        int result = dijkstra(0, 0);
        System.out.println(result);
        // bw.write(result);

        bw.close();
        br.close();
    }

    public static int dijkstra(int rowIndex, int colIndex) {
        int dist [][] = new int[rowSize][colSize];
        init(dist, 10000);

        dist[rowIndex][colIndex] = map[rowIndex][colIndex];
        PriorityQueue<Element> pq = new PriorityQueue<>(new Comparator<Element>() {
            public int compare(Element o1, Element o2) {
                return Integer.compare(o1.weight, o2.weight);
            }
        });

        pq.offer(new Element(rowIndex, colIndex, dist[rowIndex][colIndex]));
        while(!pq.isEmpty()) {
            Element el = pq.poll();
            int currRow = el.rowIndex;
            int currCol = el.colIndex;
            int currWeight = el.weight;
            for(int i=0; i<4; i++) {
                int changedRow = currRow + dRow[i];
                int changedCol = currCol + dCol[i];
                if(isInRange(changedRow, changedCol) && 
                    (currWeight + map[changedRow][changedCol] < dist[changedRow][changedCol])) {
                        dist[changedRow][changedCol] = currWeight + map[changedRow][changedCol];
                        pq.offer(new Element(changedRow, changedCol, dist[changedRow][changedCol]));
                     }
            }
        }
        return dist[rowSize-1][colSize-1];
    }

    public static void init(int [][] dist, int INF) {
        for(int i=0; i<rowSize; i++) {
            for(int j=0; j<colSize; j++) {
                dist[i][j] = INF;
            }
        }
    }

    public static boolean isInRange(int row, int col){
		return (0<=row&&row<rowSize)&&(0<=col&&col<colSize);
	}
}

class Element {
    int rowIndex;
    int colIndex;
    int weight;
    public Element(int rowIndex, int colIndex, int weight) {
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
        this.weight = weight;
    }
}
