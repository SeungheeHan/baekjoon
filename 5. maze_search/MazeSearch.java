import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

// 문제: https://www.acmicpc.net/problem/2178
// 풀이1: https://jaejin89.tistory.com/31 (여기에 우선순위 큐 쓰는 풀이도 있음)
// 풀이2: https://velog.io/@juhyun7793/%EB%B0%B1%EC%A4%80-2178-%EB%AF%B8%EB%A1%9C%EC%B0%BE%EA%B8%B0-Java
// BFS에 대해: https://velog.io/@skyepodium/BFS%EB%8A%94-%EB%82%AF%EC%84%A4%EC%96%B4%EC%84%9C
// Array.fill 에 대해: https://zetawiki.com/wiki/%EC%9E%90%EB%B0%94_Arrays.fill()
class MazeSearch {
    static Queue<Node> q = new LinkedList<>();
    static int res;
    static int[] drow = {0,0,1,-1};
    static int[] dcol = {1,-1,0,0};
    static int nrow;
    static int ncol;

    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int nrow = Integer.parseInt(st.nextToken());
        int ncol = Integer.parseInt(st.nextToken());

        int [][] map = new int[nrow][ncol];
        int [][] visited = new int[nrow][ncol];

        for(int i=0; i<nrow; i++) {
            String line = br.readLine();
            for(int j=0; j<ncol; j++) {
                map[i][j] = line.charAt(j) - '0';
            }
        }

        for(int i=0; i<nrow; i++) {
            Arrays.fill(visited[i], Integer.MAX_VALUE);
        }

        visited[0][0] = 1;
        q.add(new Node(0, 0, 1));

        while(!q.isEmpty()) {
            Node cur = q.poll();
            if(cur.rowIndex == nrow-1 && cur.colIndex == ncol) {
                res = cur.cnt;
                break;
            }

            for(int i=0; i<4; i++) {
                int newRow = cur.rowIndex + drow[i];
                int newCol = cur.colIndex + dcol[i];
                if(isRange(newRow, newCol) && visited[newRow][newCol]>cur.cnt+1 && map[newRow][newCol]==1) {
                    visited[newRow][newCol] = cur.cnt + 1;
                    q.add(new Node(newRow, newCol, cur.cnt+1));
                }
            }
        }
        System.out.println(res);
    }

    static Boolean isRange(int row, int col) {
        if (row >= 0 && row < nrow && col >= 0 && col < ncol) {
            return true;
        } else {
            return false;
        }
    }
}

class Node {
    int rowIndex;
    int colIndex;
    int cnt;

    Node(int x, int y, int cnt) {
        this.rowIndex = x;
        this.colIndex = y;
        this.cnt = cnt;
    }
}
