import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;


class BuyingCards {

    // 문제: https://www.acmicpc.net/problem/16194
    // 풀이: https://wellohorld.tistory.com/m/130
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

        String line = br.readLine();
        int N = Integer.parseInt(line);

        line = br.readLine();
        StringTokenizer st = new StringTokenizer(line);
        int[] P = new int[N+1];
        for(int i=1; i<=N; i++) {
            P[i] = Integer.parseInt(st.nextToken());
            for(int j=1; j<i; j++) {
                P[i] = Math.min(P[i], P[j] + P[i-j]);
            }
        }

        bw.write(P[N] + '\n');
        bw.flush();

        br.close();
        bw.close();
    }
}